const AccessControl = require('accesscontrol');
const ac = new AccessControl();

ac.grant('student')               
    .deleteOwn('account')
    .updateOwn('account')
    .readAny('random-number');

ac.grant('guest')                   
    .createAny('account');

module.exports = ac;

