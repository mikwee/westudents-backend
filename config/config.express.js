const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
/** 
const httpStatus = require('http-status');
const expressWinston = require('express-winston');
const expressValidation = require('express-validation');
*/
const helmet = require('helmet');


// const winstonInstance = require('./config.winston');
const routes = require('../index.route');
const config = require('./config');
// const APIError = require('../server/lib/APIError');

const app = express();

// parse body params and attach them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());


// mount all routes on /api path
app.use('/v2', routes);

app.get('/', function (req, res) {
  res.sendStatus(200);
});

app.get('/healthcheck', function (req, res) {
  res.sendStatus(200);
});


if (config.env === 'development') {
  app.use(logger('dev'));
}
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('API Not Found');
  err.status = 404;
  next(err);
});


/* Error handlers */

if (config.env === 'development') {
  // development error handler
  // will print stacktraceF
  app.use(function (err, req, res, next) {
    res.status(err.status || 500).json({
      status: 'fail',
      message: err.message,
      error: err
    });
  });

} else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500).json({
      status: 'fail',
      message: err.message,
      error: {}
    });
  });

}

module.exports = app;