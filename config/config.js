const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({

  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),

  PORT: Joi.number()
    .default(4040),

  JWT_SECRET: Joi.string().required().description('JWT Secret required to sign'),

  MONGOOSE_DEBUG: Joi.boolean()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      then: Joi.boolean().default(true),
      otherwise: Joi.boolean().default(false)
    }),
  MONGO_HOST: Joi.string()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      then: Joi.string().default(Joi.ref('MONGO_DEV_HOST')),
      otherwise: Joi.string().default(Joi.ref('MONGO_PROD_HOST'))
    }).description('Mongo DB host url'),
  MONGO_PORT: Joi.number()
    .default(27017),
  MONGO_DB: Joi.string().required(),

  GCLOUD_PROJECT: Joi.string().required(),
  CLOUD_BUCKET: Joi.string()


}).unknown().required();

// validate envVars with joi
const {
  error,
  value: envVars
} = Joi.validate(process.env, envVarsSchema);

// catch any config error
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

// create config file
const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  jwtSecretObj: {
    secret: envVars.JWT_SECRET
  },
  mongo: {
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT
  },
  gcloud: {
    project: envVars.GCLOUD_PROJECT,
    bucket: envVars.CLOUD_BUCKET
  }
};

module.exports = config;