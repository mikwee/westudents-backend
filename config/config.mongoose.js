const util = require('util');
const mongoose = require('mongoose');
const debug = require('debug')('westudents-backend:index');

const config = require('./config');

// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;

// connect to mongo db
const mongoUri = config.mongo.host;

mongoose.connect(mongoUri, {useMongoClient: true});

mongoose.connection.on('error', () => {
    throw new Error(`Unable to connect to database: ${mongoUri}`);
});

mongoose.connection.once('open', () => {
    console.log('mongo connection setup!');
});


// print mongoose logs in dev env
if (config.mongooseDebug) {
    mongoose.set('debug', (collectionName, method, query, doc) => {
        debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
    })
}