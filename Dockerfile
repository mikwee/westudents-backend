FROM node:8.10.0-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm i --production --silent && mv node_modules ../
#RUN npm i
COPY . .
EXPOSE 3000
CMD npm start