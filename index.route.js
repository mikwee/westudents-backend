const express = require('express');

const profileUser = require('./server/lib/profiler.js');
const authRoutes = require('./server/auth/auth.route');
const accountRoutes = require('./server/account/account.route');

const router = express.Router();

/** Profile user, get authentication and authorizations if present */
router.use(profileUser);

/**
 *  MOUNT auth routes at -
 *  api/auth 
 */
router.use('/auth', authRoutes);

/**
 *  MOUNT account routes at -
 *  api/account 
 */
router.use('/account', accountRoutes);

/** 
 * GET api/health-check -
 * Check service health 
 */
router.get('/health-check', (req, res) => res.send('OK'));


module.exports = router;