const express = require('express');

const profileUser = require('./server/lib/profiler.js');

const accountRoutes = require('./server/account/account.route');
const schoolRoutes = require('./server/school/school.route');
const classRoutes = require('./server/class/class.route');

const router = express.Router();

/** Profile user, get authentication and permissions if present */
router.use(profileUser);

/**
 *  MOUNT account routes at -
 *  api/account 
 */
router.use('/account', accountRoutes);

/**
 *  MOUNT school routes at -
 *  v2/schools
 */
router.use('/schools', schoolRoutes);

/**
 *  MOUNT class routes at -
 *  v2/classes
 */
router.use('/classes', classRoutes);



module.exports = router;