const Account = require('./account.model');
const APIResponse = require('../lib/APIResponse');


/**
 * Create new user
 * @returns {Account}
 */
function create(req, res, next) {

  let user = Account(req.body);

  // Is user requesting permissions ?
  if (req.body.role && req.body.role !== 'student') {
    // TODO handle user asking permissions
  }

  user.save()
    .then(savedUser => res.json(APIResponse.create(savedUser)))
    .catch(e => next(e));
}

/**
 * Load Account
 * @returns {Account}
 */
function load(req, res, next) {
  // req.user is assigned by jwt middleware if valid token is provided
  Account.get(req.user._id)
    .then(user => res.json(APIResponse.create(user)))
    .catch(e => next(e));
}

/**
 * Update existing user
 * @returns {Account}
 */
function update(req, res, next) {

  // Was an image uploaded? If so, we'll use its public URL in cloud storage.
  if (req.file && req.file.cloudStoragePublicUrl) {
    req.body.avatar = req.file.cloudStoragePublicUrl;
  }

  Account.findOneAndUpdate(req.user._id, req.body, {new: true})
    .then(userUpdated => res.json(APIResponse.create(userUpdated)))
    .catch(e => next(e));
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(req, res, next) {
  const {
    limit = 50, skip = 0
  } = req.query;
  Account.list({
      limit,
      skip
    })
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {Account}
 */
function remove(req, res, next) {

  Account.get(req.user._id)
    .then(user => {
      return user.remove()
    })
    .then(deletedUser => res.json(APIResponse.create(deletedUser)))
    .catch(e => next(e));
}


module.exports = {
  create,
  load,
  update,
  list,
  remove
};