const express = require('express');
const validate = require('express-validation');

const imageUpload = require('../lib/image-upload');

const auth = require('../auth/auth.helpers');
const perm = require('../perm/perm.helper');

const accountCtrl = require('./account.controller');
const v = require('./account.validation');

const router = express.Router();
/** MOUNT v2/account */
router.route('/')

  /** POST /api/account - Create new user */
  .post(validate(v.createAccount), accountCtrl.create)

  /** GET /api/account - Get account details  - Protected route - */
  .get(auth, perm, accountCtrl.get)
  
  /** PATCH /api/account - Update account details  - Protected route - */
  .patch(auth, perm, accountCtrl.update)

  /** DELETE /api/account - Delete account details  - Protected route - */
  .delete(auth, perm, accountCtrl.remove);

/** MOUNT v2/account/avatar */
router.route('/avatar')

  /** POST /api/account/avatar - Upload or Update account avatar */
  .post(auth, perm, imageUpload, accountCtrl.update);

/** MOUNT v2/account:accountId */
router.route('/:accountId')

  /** GET - Get account details  - Protected route - */
  .get(auth, perm, accountCtrl.get)

  /** PATCH - Update account details  - Protected route - */
  .patch(auth, perm, accountCtrl.update)

  /** DELETE - Delete account details  - Protected route - */
  .delete(auth, perm, accountCtrl.remove);


/** Load account when API with accountId route parameter is hit */
router.param('accountId', accountCtrl.load);


module.exports = router;