const express = require('express');
const validate = require('express-validation');

const paramValidation = require('../lib/param-validation');
const imageUpload = require('../lib/image-upload');
const auth = require('../auth/auth.helpers');
const perm = require('../perm/perm.helper')

const accountCtrl = require('./account.controller');

const router = express.Router();

router.route('/')

  /** POST /api/account - Create new user */
  .post(validate(paramValidation.createAccount), accountCtrl.create)

  /** GET /api/account - Get account details  - Protected route - */
  .get(auth, perm, accountCtrl.load)

  /** PATCH /api/account - Update account details  - Protected route - */
  .patch(auth, perm, accountCtrl.update)

  /** DELETE /api/account - Delete account details  - Protected route - */
  .delete(auth, perm, accountCtrl.remove);


router.route('/avatar')

  /** POST /api/account/avatar - Upload or Update account avatar */
  .post(auth, perm, imageUpload, accountCtrl.update);

module.exports = router;