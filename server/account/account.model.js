const Promise = require('bluebird');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../lib/APIError');

/**
 * Account Schema
 */
const AccountSchema = new mongoose.Schema({

  /* First Name */
  first_name: {
    type: String,
    required: true
  },
  /* Last Name */
  last_name: {
    type: String,
    required: true
  },
  /* Date of birth */
  dob: {
    type: Date
  },
  /* Gender */
  gender: {
    type: String,
    required: true
  },
  /* Url to avatar Image */
  avatar: {
    type: String,
    default: undefined
  },
  /* Email */
  email: {
    type: String,
    lowercase: true,
    required: true
  },
  /* Mobile Code */
  mobile_cc: {
    type: Number,
    default: undefined
  },
  /* Mobile Number */
  mobile_number: {
    type: Number,
    required: true,
    match: [/^[1-9][0-9]{8}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']

  },
  /* School */
  school: {
    type: String,
    required: true
  },
  /* Class year */
  class_year: {
    type: Number,
    required: true
  },
  /* Class year */
  class_section: {
    type: String,
    required: true
  },
  /* User role */
  role: {
    type: String,
    default: 'student'
  },
  /* User role  is approved ? */
  role_approved: {
    type: Boolean,
    default: true
  },

  /* Is activated ? */
  activated: {
    type: Boolean,
    default: false
  },

  account_status: {
    type: Number,
    default: 0
  }

});

/**
 * TODO Add
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
AccountSchema.method({});

/**
 * Statics
 */
AccountSchema.statics = {

  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get user
   * @param {String} email - The mail of user.
   * @returns {Promise< Account , APIError>}
   */
  getByMail(email) {

    return this.findOne({
        'email': email
      })
      .limit(1)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists with email ' + email, httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({
    skip = 0,
    limit = 50
  } = {}) {
    return this.find()
      .sort({
        createdAt: -1
      })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }

};

/**
 * @typedef Account
 */
module.exports = mongoose.model('User', AccountSchema);