const Joi = require('joi');


module.exports = {

  // POST /v2/school
  createSchool: {
    body: {
      code: Joi.string().required(),
      name: Joi.string().required(),
      speciality: Joi.string().regex(/^[a-zA-Z]+$/).required(), // only letters
      type: Joi.string().regex(/^[a-zA-Z\s]+$/).required(), // only letters
      avatar: Joi.string().optional(),

      location: Joi.object().keys({
        area: Joi.string().regex(/^[a-zA-Z\s]+$/).optional(), // only letters
        region: Joi.string().regex(/^[a-zA-Z\s]+$/).optional(), // only letters & space
        province: Joi.string().regex(/^[a-zA-Z\s]+$/).optional(), // only letters & space
        city: Joi.string().regex(/^[a-zA-Z\s]+$/).optional(), // only letters & space
        city_code: Joi.string().optional(),
        address: Joi.string().optional(),
        cap: Joi.number().optional() // Number
      }),


      contact: Joi.object().keys({
        email: Joi.string().email().optional(), // valid email
        email_pec: Joi.string().email().optional(), // valid email
        website: Joi.string().optional()
      })
      
      /** 
      figures: Joi.object().keys({
        ambassador: Joi.string().optional(),
        istitute_rappresentant: Joi.string().optional()  
      })
      */
    }
  }

}