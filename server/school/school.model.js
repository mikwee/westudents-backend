const httpStatus = require('http-status');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const APIError = require('../lib/APIError');

/**
 * School Schema
 */
const SchoolSchema = new Schema({

  /* Codice scuola */
  code: {
    type: String,
    required: true
  },
  /* Denominazione scuola */
  name: {
    type: String,
    required: true
  },
  /* Descrizione scuola */
  speciality: {
    type: String,
    required: true
  },
  /* Tipologia */
  type: {
    type: String,
    required: true
  },
  /* Url to school Image */
  image: {
    type: String,
    default: undefined
  },

  /* Location Object */
  location: {
    area: String,
    region: String,
    province: String,
    city: String,
    city_code: String,
    address: String,
    cap: Number
  },

  /* Contact Object */
  contact: {
    /* Email */
    email: {
      type: String,
      default: 'Non disponibile'
    },
    /* Email pec */
    email_pec: {
      type: String,
      default: 'Non disponibile'
    },
    /* Website */
    website: {
      type: String,
      default: 'Non disponibile'
    }
  },

  /* Figures Object */
  figures: {

    /* Ambassador */
    ambassador: {
      type: Schema.ObjectId,
      ref: 'User',
      default: undefined
    },
    /* Rappresentante */
    istitute_rappresentant: {
      type: Schema.ObjectId,
      ref: 'User',
      default: undefined
    }

  }

});

/**
 * Statics
 */
SchoolSchema.statics = {

  /**
   * Get school
   * @param {ObjectId} id - The objectId of school.
   * @returns {Promise<School, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((school) => {
        if (school) {
          return school;
        }
        const err = new APIError('No such school exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * Get school
   * @param {String} email - The mail of school.
   * @returns {Promise< School , APIError>}
   */
  getByMail(email) {

    return this.findOne({
        'email': email
      })
      .limit(1)
      .exec()
      .then((school) => {
        if (school) {
          return school;
        }
        const err = new APIError('No such school exists with email ' + email, httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /** List schools */
  list(query) {
    return this.find(query)
      // .sort({"created_at":-1})
      // .skip(skip)
      .limit(20)
      .exec();
  }

};

/**
 * @typedef School
 */
module.exports = mongoose.model('School', SchoolSchema);