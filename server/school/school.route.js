const express = require('express');
const validate = require('express-validation');

const imageUpload = require('../lib/image-upload');
const auth = require('../auth/auth.helpers');
const perm = require('../perm/perm.helper');

const schoolCtrl = require('./school.controller');
const v = require('./school.validation');

const router = express.Router();

/** MOUNT v2/schools */
router.route('/')

  /** POST - Create new school  - Protected route */
  .post(validate(v.createSchool), auth, perm, schoolCtrl.create);


/** MOUNT v2/schools/list */
router.route('/list')

  /** GET - Get school list with certain query */
  .get(auth, perm, schoolCtrl.list);

/** MOUNT v2/schools:schoolId */
router.route('/:schoolId')

  /** GET - Get school details  - Protected route - */
  .get(auth, perm, schoolCtrl.get)

  /** PATCH - Update school details  - Protected route - */
  .patch(auth, perm, schoolCtrl.update)

  /** DELETE - Delete school details  - Protected route - */
  .delete(auth, perm, schoolCtrl.remove);


/** MOUNT v2/schools/avatar */
router.route('/:schoolId/avatar')

  /** POST - Upload or update school avatar */
  .post(auth, perm, imageUpload, schoolCtrl.update);


/** MOUNT v2/schools/classes */
router.route('/:schoolId/classes')
  /** GET - Get list of classes for that school */
  .get(auth, perm, schoolCtrl.getClasses);


/** Load schools when API with schoolId route parameter is hit */
router.param('schoolId', schoolCtrl.load);

module.exports = router;