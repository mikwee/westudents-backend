const httpStatus = require('http-status');

const APIResponse = require('../lib/APIResponse');
const APIError = require('../lib/APIResponse');

const ClassesWrapper = require('../class/classWrapper.model');
const School = require('./school.model');


/** Create new School */
function create(req, res, next) {

  let school = School(req.body);

  school.save()
    .then(savedSchool => new APIResponse(res).success(savedSchool))
    .catch(e => next(e));
}

/** Load school and append to req */
function load(req, res, next, id) {
  School.get(id)
    .then(school => {
      req.school = school;
      return next();
    })
    .catch(e => next(e));
}

/** Get a School */
function get(req, res, next) {
  // req.school is assigned by jwt middleware if valid token is provided
  School.get(req.school._id)
    .then(school => res.json(APIResponse.create(school)))
    .catch(e => next(e));
}

/** Update existing school */
function update(req, res, next) {

  // Was an image uploaded? If so, we'll use its public URL in cloud storage.
  if (req.file && req.file.cloudStoragePublicUrl) {
    req.body.avatar = req.file.cloudStoragePublicUrl;
  }

  School.findOneAndUpdate({
      _id: req.school._id
    }, req.body, {
      new: true
    })
    .then(schoolUpdated => res.json(APIResponse.create(schoolUpdated)))
    .catch(e => next(e));
}

/** Get school list */
function list(req, res, next) {

  // Get the request query object 
  let q = req.query;

  if (!q)
    return new APIError('Should have at least one query', httpStatus.BAD_REQUEST, true);

  let query = {};

  // Search by name
  if (q.name)
    query.name = new RegExp(q.name, 'i');

  // Schools in a region
  if (q.region)
    query['location.region'] = new RegExp(q.region, 'i');

  // Schools in a city
  if (q.city)
    query['location.city'] = new RegExp(q.city, 'i');


  School.list(query)
    .then(schools => new APIResponse(res).success(schools))
    .catch(e => next(e));
}

/** Delete school */
function remove(req, res, next) {

  School.get(req.school._id)
    .then(school => {
      return school.remove()
    })
    .then(deletedschool => res.json(APIResponse.create(deletedschool)))
    .catch(e => next(e));
}

/** Get a School classes */
function getClasses(req, res, next) {
  let query = {
    school: req.school._id
  }
  ClassesWrapper.list(query)
    .then(classes => new APIResponse(res).success(classes))
    .catch(e => next(e));
}


module.exports = {
  create,
  get,
  load,
  update,
  list,
  remove,
  getClasses
};