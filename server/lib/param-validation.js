const Joi = require('joi');


const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const dateRegex = /^\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/;

module.exports = {


  // POST /api/account
  createAccount: {
    body: {
      first_name: Joi.string().regex(/^[a-zA-Z]+$/).required(), // only letters
      last_name: Joi.string().regex(/^[a-zA-Z]+$/).required(), // only letters
      dob: Joi.string().regex(dateRegex).optional(), // valid date
      gender: Joi.string().regex(/^(M|F)$/).required(), // only M or F
      avatar: Joi.string().optional(),

      email: Joi.string().regex(emailRegex).required(), // only valid emails
      mobile_cc: Joi.string().regex(/^(\+?\d{1,3}|\d{1,4})$/).optional(), // only valid country codes
      mobile_number: Joi.string().regex(/^[1-9][0-9]{9}$/).required(), // only 10 numbers starting from 1 at least

      city: Joi.string().regex(/^[a-zA-Z]+$/).optional(), // only letters
      address: Joi.string().optional(), // only letters

      school: Joi.string().required(),
      class_year: Joi.number().required(),
      class_section: Joi.string().required()
    }
  },


  
  // POST /api/account/addresses
  createAddress: {
    body: {
      address_1: Joi.string().required(), // text
      address_2: Joi.string().required(), // text
      city: Joi.string().regex(/^[a-zA-Z]+$/).required(), // only text
      post_code: Joi.string().regex(/^[1-9][0-9]{5}$/).required(), // 5 digit numbers
      county: Joi.string().regex(/^[a-zA-Z]+$/).required(), // only valid country name
      country: Joi.string().regex(/^[1-9][0-9]{3}$/).required() // only 2 letters
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().regex(emailRegex).required()
    }
  },


  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  }




};