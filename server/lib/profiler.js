const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

const Account = require('../account/account.model');

var JWT_PUBKEY_FOR_ACCESS_TOKEN = fs.readFileSync(path.join(__dirname, '/../../keys/secp256k1-public.pem'));

/**
 * This function appends to the request the user authentification and authorization
 */
function profileUser(req, res, next) {

    let user = {
        role: 'guest',
        isAuthenticated: false
    }

    /** Check if there is an authentication token */
    if (req.headers.authorization) {
        let bearerToken = req.headers.authorization.replace('Bearer ', '');

        try {
            /** Decode token payload */
            let payload = jwt.verify(bearerToken, JWT_PUBKEY_FOR_ACCESS_TOKEN, {
                algorithms: ['ES256']
            });

            /** Append Token payload to request */
            let token = {
                expiryDate: new Date(payload.exp * 1000),
                cid: payload.cid
            };

            req.token = token;

            return Account
                .findById(payload.uuid)
                .then((user) => {
                    user.isAuthenticated = true;
                    /** Append whole user to request */
                    req.user = user;
                    return next();
                })
                .catch(function (err) {
                    console.log('getAccessToken - Err: ' + err)
                })

        } catch (err) {
            // Error decompiling code
        }
    }

    /** Append user to request */
    req.user = user;
    return next();
}

module.exports = profileUser;