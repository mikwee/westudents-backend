const config = require('../../config/config');
const jwt = require('jsonwebtoken');


/**
 * This function appends to the request the user authentification and authorization
 */
function profileUser(req, res, next) {

    let user = {
        role: 'guest',
        isAuthenticated: false
    }

    /** Check if there is an authentication token */
    if (req.headers.authorization) {
        let token = req.headers.authorization.replace('Bearer ', '');

        /** Check if there token is correct and append user */
        try {
            user = jwt.verify(token, config.jwtSecret);
            user.isAuthenticated = true;
        } catch (err) {
            // Error decompiling code
        }
    }

    /** Append user to request */
    req.user = user;
    return next();
}

module.exports = profileUser;