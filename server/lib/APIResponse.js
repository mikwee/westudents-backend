
class APIResponse {

  /**
   * Creates an API standard response. WORK IN PROGRESS
   * @param {number} status - HTTP status code of error.
   * @param {string} message - Error message.
   * @param {Object} data - the data we want to send.
   */
  static create(dataObj) {
    return {
      status: APIResponse.SUCCESS,
      data: dataObj
    };
  }

}

APIResponse.SUCCESS = 'success';


module.exports = APIResponse;