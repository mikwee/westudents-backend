class APIResponse {

  constructor(res) {
    this.res = res;
  }

  success(data) {
    this.res.json({
      status: 'success',
      data: data
    });
  }

  /** DEPRECATED */
  static create(dataObj) {
    return {
      status: APIResponse.SUCCESS,
      data: dataObj
    };
  }

}

APIResponse.SUCCESS = 'success';


module.exports = APIResponse;