const Multer = require('multer');
const Storage = require('@google-cloud/storage');
const config = require('../../config/config');

/**
 * Filters uploaded files to be only images
 */
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

/**
 * Create Multer istance
 */
const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 1024 * 1024 * 5 // images no larger than 5 MB
    },
    fileFilter: fileFilter
});

/**
 * Create GCStorage istance
 */
const CLOUD_BUCKET = config.gcloud.bucket;

/**
 * Create GCStorage istance
 */
const storage = Storage({
  projectId: config.gcloud.project
});
const bucket = storage.bucket(CLOUD_BUCKET);

/** Express middleware that will automatically pass uploads to Cloud Storage.
 *  
 *  req.file is processed and will have two new properties:
 *  ``cloudStorageObject`` the object name in cloud storage.
 *  ``cloudStoragePublicUrl`` the public url to the object. 
 */
function sendUploadToGCS(req, res, next) {
   
    if (!req.file) {
        return next();
    }

    const gcsname = Date.now() + req.file.originalname;
    const file = bucket.file(gcsname);

    const stream = file.createWriteStream({
        metadata: {
            contentType: req.file.mimetype
        }
    });

    stream.on('error', (err) => {
        req.file.cloudStorageError = err;
        next(err);
    });

    stream.on('finish', () => {
        req.file.cloudStorageObject = gcsname;
        file.makePublic().then(() => {
            req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
            next();
        });
    });

    stream.end(req.file.buffer);
}

/** Returns the public, anonymously accessable URL to a given Cloud Storage
 *  object.The object's ACL has to be set to public read.
 */
function getPublicUrl(filename) {
    return `https://storage.googleapis.com/${CLOUD_BUCKET}/${filename}`;
}

module.exports = [multer.single('image'), sendUploadToGCS];