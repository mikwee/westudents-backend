const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');

const APIError = require('../lib/APIError');
const config = require('../../config/config');
const Account = require('../account/account.model');

/**
 * Returns jwt token if valid mail is provided
 */
function login(req, res, next) {

  // TODO Ideally we want to do another identification processes, but for now this is enough
  Account.getByMail(req.body.email)
    .then((user) => {

      /** create JWT token payload */
      const token = jwt.sign({
        _id: user._id,
        role: user.role
      }, config.jwtSecret);

      return res.json({
        status: 'success',
        token: token
      });

    })
    .catch(e => {
      const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
      return next(err);
    });


}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 */
function getRandomNumber(req, res) {
  
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

module.exports = {
  login,
  getRandomNumber
};