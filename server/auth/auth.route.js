const express = require('express');
const validate = require('express-validation');

const paramValidation = require('../lib/param-validation');
const authCtrl = require('./auth.controller');
const auth = require('./auth.helpers');
const perm = require('../perm/perm.helper')

const router = express.Router(); 

/** 
 * POST /api/auth/login - 
 * Returns token for the given email, PROVVISORY 
 */
router.route('/login')
  .post(validate(paramValidation.login), authCtrl.login);

/** 
 * GET /api/auth/random-number -
 * Protected route, needs auth and permission
 */
router.route('/random-number')
  .get(auth, perm, authCtrl.getRandomNumber);


module.exports = router;