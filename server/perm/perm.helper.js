const httpStatus = require('http-status');

const ac = require('../../config/config.permissions')
const APIError = require('../lib/APIError');


/**
 * Checks if the requester has the right to access a certain resource
 */
function check(req, res, next) {
    let role = req.user.role;
    let action = req.method;
    let resource = req.path;

    var isAllowed = false;
    switch (action) {
        case 'GET':
            isAllowed = ac.can(role).readAny(resource);
            break;
        default:
            isAllowed = ac.can(role).readAny(resource);
            break;
    }


    if (isAllowed) {
        next();
    } else {
        const err = new APIError('Permission error', httpStatus.UNAUTHORIZED, true);
        return next(err);
    }


}

module.exports = check;