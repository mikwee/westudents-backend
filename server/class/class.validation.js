const Joi = require('joi');


module.exports = {

  // POST /v2/classes
  createClass: {
    body: {
      code: Joi.string().optional(),
      year: Joi.number().required(),
      section: Joi.string().required(),
      school: Joi.string().required()
    }
  }

}