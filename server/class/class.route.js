const express = require('express');
const validate = require('express-validation');

const imageUpload = require('../lib/image-upload');
const auth = require('../auth/auth.helpers');
const perm = require('../perm/perm.helper');

const classCtrl = require('./class.controller');
const v = require('./class.validation');

const router = express.Router();

/** MOUNT v2/classes */
router.route('/')

  /** POST - Create new class  - Protected route */
  .post(validate(v.createClass), auth, perm, classCtrl.create);

/** MOUNT v2/classes:classId */
router.route('/:classId')

  /** GET - Get classes details  - Protected route - */
  .get(auth, perm, classCtrl.get)

  /** PATCH - Update class details  - Protected route - */
  .patch(auth, perm, classCtrl.update)

  /** DELETE - Delete class details  - Protected route - */
  .delete(auth, perm, classCtrl.remove);


/** MOUNT v2/classes/avatar */
router.route('/:classId/avatar')

  /** POST - Upload or update class avatar */
  .post(auth, perm, imageUpload, classCtrl.update);


/** Load class when API with classId route parameter is hit */
router.param('classId', classCtrl.load);

module.exports = router;