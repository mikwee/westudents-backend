const httpStatus = require('http-status');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const APIError = require('../lib/APIError');

/**
 * Class Core Schema
 */
const ClassCoreSchema = new Schema({

  /* Class Object reference */
  class: {
    type: Schema.ObjectId,
    ref: 'Class'
  },

  /* Url to Class Image */
  image: {
    type: String,
    default: undefined
  },

  /* Class student list */
  students: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],

  /* Class representants list */
  class_representants: [{
    type: Schema.ObjectId,
    ref: 'User'
  }]


});

/**
 * Statics
 */
ClassCoreSchema.statics = {

  /**
   * Get Class
   * @param {ObjectId} id - The objectId of Class.
   * @returns {Promise<Class, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((Class) => {
        if (Class) {
          return Class;
        }
        const err = new APIError('No such Class exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /** List Classs with a given query object */
  list(query) {
    return this.find(query)
      .limit(20)
      .exec();
  }

};

/**
 * @typedef ClassCore
 */
module.exports = mongoose.model('ClassCore', ClassCoreSchema);