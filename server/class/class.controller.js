const APIResponse = require('../lib/APIResponse');

const Class = require('./classWrapper.model');


/** Create new Class */
function create(req, res, next) {

  let classe = Class(req.body);

  classe.save()
    .then(savedClass => new APIResponse(res).success(savedClass))
    .catch(e => next(e));
}

/** Load Class and append to req. */
function load(req, res, next, id) {
  Class.get(id)
    .then(classe => {
      req.classe = classe;
      return next();
    })
    .catch(e => next(e));
}

/** Get Class */
function get(req, res, next) {
  Class.get(req.classe._id)
    .then(classe => new APIResponse(res).success(classe))
    .catch(e => next(e));
}

/** Update existing Class */
function update(req, res, next) {

  // Was an image uploaded? 
  if (req.file && req.file.cloudStoragePublicUrl)
    req.body.image = req.file.cloudStoragePublicUrl;

  Class.findOneAndUpdate({
      _id: req.classe._id
    }, req.body, {
      new: true
    })
    .then(classeUpdated => new APIResponse(res).success(classeUpdated))
    .catch(e => next(e));
}

/** Delete classe */
function remove(req, res, next) {

  Class.get(req.classe._id)
    .then(classe => {
      return classe.remove()
    })
    .then(deletedclasse => new APIResponse(res).success(deletedclasse))
    .catch(e => next(e));
}


module.exports = {
  create,
  get,
  load,
  update,
  remove
};