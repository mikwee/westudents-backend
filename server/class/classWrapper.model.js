const httpStatus = require('http-status');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const APIError = require('../lib/APIError');

/**
 * Class Schema
 */
const ClassSchema = new Schema({


    /* Url to Class Image */
    image: {
        type: String,
        default: undefined
    },

    /* School Object reference */
    school: {
        type: Schema.ObjectId,
        required: true,
        ref: 'School'
    },

    /* Class year */
    year: {
        type: Number,
        required: true
    },

    /* Class section */
    section: {
        type: String,
        required: true
    },

    /* Class code */
    code: {
        type: String,
        default: 'Non disponibile'
    },

    /* The flexible part */
    core_class: {
        type: Schema.ObjectId,
        ref: 'ClassCore',
        default: undefined
    }

});

/**
 * Statics
 */
ClassSchema.statics = {

    /**
     * Get Class
     * @param {ObjectId} id - The objectId of Class.
     * @returns {Promise<Class, APIError>}
     */
    get(id) {
        return this.findById(id)
            .exec()
            .then((Class) => {
                if (Class) {
                    return Class;
                }
                const err = new APIError('No such Class exists!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },

    /** List Classs with a given query object */
    list(query) {
        return this.find(query)
            .limit(20)
            .exec();
    }

};

/**
 * @typedef Class
 */
module.exports = mongoose.model('ClassWrapper', ClassSchema);